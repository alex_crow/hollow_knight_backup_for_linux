# hollow_knight_backup_for_linux

GNU/Linux Bash scripts for automatically backing up Hollow Knight saved game files. This is especially useful for the game's Steel Soul mode.
