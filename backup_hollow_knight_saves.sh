#!/bin/bash

INPUT_DIR="${HOME}/.config/unity3d/Team Cherry/Hollow Knight"
OUTPUT_DIR="${HOME}/Games/bkp/hollow_knight/save_files"

inotifywait                \
  --monitor "${INPUT_DIR}" \
  --format  "%w%f"         \
  --event   modify         \
  --exclude "Player.log" |
  while read FILE; do
    TIMESTAMP=$(date "+%Y%m%d%H%M")

    mkdir -p "${OUTPUT_DIR}/${TIMESTAMP}"

    echo
    echo "File ${FILE} was modified in ${INPUT_DIR}..."
    echo "Backing-up file..."
    cp "${INPUT_DIR}"/* "${OUTPUT_DIR}/${TIMESTAMP}"
  done

RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]; then
  echo
  echo "[FAILURE]: Something went wrong when attempting to back-up Hollow Knight save-files..."
else
  echo "[SUCCESS]: Hollow Knight save-files backed up successfully!"
fi

exit ${RETURN_CODE}
